FROM openjdk:11-jdk
COPY target/*.jar department-service.jar
ENTRYPOINT ["java", "-jar", "/department-service.jar"]
EXPOSE 8082