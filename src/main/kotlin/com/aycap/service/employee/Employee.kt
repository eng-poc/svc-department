package com.aycap.service.employee

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.*

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
data class Employee (
    @Id
    @Column(name = "EMPLOYEE_ID")
    var employeeId: String,

    @Column(name = "NAME")
    var name: String,
    @Column(name = "AGE")
    var age: Int = 0,
    @Column(name = "POSITION")
    var position: String,
    @Column(name = "ORGANIZATION_ID")
    var organizationId: String,
    @Column(name = "DEPARTMENT_ID")
    var departmentId: String
)