package com.aycap.service.employee

import com.aycap.service.payload.FindEmployeeResponse
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.cloud.openfeign.FeignClient

@FeignClient(name = "employee-service")
interface EmployeeClient {

    @GetMapping("/employees/department/{departmentId}")
    fun findEmployeeByDepartmentId(@PathVariable departmentId: String): List<FindEmployeeResponse>

}