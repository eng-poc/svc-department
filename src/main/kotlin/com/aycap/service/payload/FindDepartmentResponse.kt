package com.aycap.service.payload

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
class FindDepartmentResponse(
    var departmentId: String,
    var name: String,
    var organiztionId: String
)