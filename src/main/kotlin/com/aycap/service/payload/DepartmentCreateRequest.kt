package com.aycap.service.payload

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
class DepartmentCreateRequest(
    var name: String,
    var organizationId: String
)