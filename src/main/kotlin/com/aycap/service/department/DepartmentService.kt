package com.aycap.service.department

import com.aycap.service.employee.EmployeeClient
import com.aycap.service.payload.DepartmentCreateRequest
import com.aycap.service.payload.DepartmentCreateResponse
import com.aycap.service.payload.DepartmentInfoResponse
import com.aycap.service.payload.FindDepartmentResponse
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand
import org.springframework.cloud.netflix.ribbon.RibbonClient
import org.springframework.stereotype.Service


@Service
class DepartmentService(
    private val departmentRepository: DepartmentRepository,
    private val employeeClient: EmployeeClient
) {

    fun findAllDepartment(): List<FindDepartmentResponse> {
        return departmentListToDTO(departmentRepository.findAll())
    }

    fun findDepartmentByOrganizationId(organizationId: String): List<FindDepartmentResponse> {
        return departmentListToDTO(departmentRepository.findByOrganizationId(organizationId))
    }

    @HystrixCommand(fallbackMethod = "findDepartmentInfoRecovery")
    fun findDepartmentInfo(departmentId: String): DepartmentInfoResponse {
        val department = departmentRepository.findByDepartmentId(departmentId.toLong())
        val employeeList = employeeClient.findEmployeeByDepartmentId(departmentId)
        return DepartmentInfoResponse(department.departmentId.toString(), department.name, department.organizationId, employeeList)
    }
    fun findDepartmentInfoRecovery(departmentId: String): DepartmentInfoResponse {
        val department = departmentRepository.findByDepartmentId(departmentId.toLong())
        return DepartmentInfoResponse(department.departmentId.toString(), department.name, department.organizationId, null)
    }

    fun addNewDepartment(req: DepartmentCreateRequest): DepartmentCreateResponse {
        val newDepartment = departmentRepository.save(
            Department(
                name = req.name,
                organizationId = req.organizationId
            )
        )
        return DepartmentCreateResponse(newDepartment.departmentId.toString(), newDepartment.name, newDepartment.organizationId)
    }

    internal fun departmentListToDTO(departmentList: List<Department>): List<FindDepartmentResponse> {
        return departmentList.map { FindDepartmentResponse(it.departmentId.toString(), it.name, it.organizationId) }
    }

}