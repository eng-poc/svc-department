package com.aycap.service.department

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.*

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
data class Department(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DEPARTMENT_ID")
    var departmentId: Long? = 0,

    @Column(name = "NAME")
    var name: String,
    @Column(name = "ORGANIZATION_ID")
    var organizationId: String
)