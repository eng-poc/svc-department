package com.aycap.service.department

import com.aycap.service.payload.DepartmentCreateRequest
import com.aycap.service.payload.DepartmentCreateResponse
import com.aycap.service.payload.DepartmentInfoResponse
import com.aycap.service.payload.FindDepartmentResponse
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/departments")
class DepartmentController(
    private val departmentService: DepartmentService
) {

    @GetMapping
    fun findAllDepartment(): List<FindDepartmentResponse> {
        return departmentService.findAllDepartment()
    }

    @GetMapping("/{departmentId}")
    fun findDepartmentById(@PathVariable departmentId: String): DepartmentInfoResponse {
        return departmentService.findDepartmentInfo(departmentId)
    }

    @GetMapping("/organization/{organizationId}")
    fun findDepartmentByOrganizationId(@PathVariable organizationId: String): List<FindDepartmentResponse> {
        return departmentService.findDepartmentByOrganizationId(organizationId)
    }

    @PostMapping(
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun addNewDepartment(@RequestBody req: DepartmentCreateRequest): DepartmentCreateResponse {
        return departmentService.addNewDepartment(req)
    }
}