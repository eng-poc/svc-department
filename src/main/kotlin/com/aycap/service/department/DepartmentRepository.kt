package com.aycap.service.department

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DepartmentRepository: JpaRepository<Department, Long> {
    fun findByOrganizationId(organizationId: String): List<Department>
    fun findByDepartmentId(departmentId: Long): Department
}